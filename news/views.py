from django.shortcuts import render, get_object_or_404
from .models import Post
# Create your views here.


def index(request):
    posts = Post.objects.all().exclude(visibility='Drafted')
    return render(request, "index.html", {'posts': posts})


def detail(request, slugs):
    full = get_object_or_404(Post, slug=slugs, visibility="Published")
    return render(request, "details.html", {'full': full})
